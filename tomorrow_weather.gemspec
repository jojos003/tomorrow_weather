# frozen_string_literal: true

require_relative 'lib/tomorrow_weather/version'

Gem::Specification.new do |spec|
  spec.name        = 'tomorrow_weather'
  spec.version     = TomorrowWeather::VERSION
  spec.summary     = 'What will the weather be like tomorrow'
  spec.description = 'What will the weather be like tomorrow'
  spec.authors     = ['Jonathan GEORGES']
  spec.email       = 'jojos003@free.fr'
  spec.required_ruby_version = '>= 2.6.0'

  spec.metadata['rubygems_mfa_required'] = 'true'

  spec.files = `git ls-files lib README.md`.split($RS)
  spec.require_paths = %w[lib]
  spec.executables = %w[tomorrow_weather]
end
