# frozen_string_literal: true

require 'tomorrow_weather/cli'

RSpec.describe TomorrowWeather::CLI do
  subject(:cli) { described_class.new(city, output: output, weather_api: weather_api) }

  let(:output) { StringIO.new }
  let(:weather_api) { instance_double('TomorrowWeather::WeatherAPI') }

  context 'when city is nil' do
    let(:city) { nil }

    it 'failed because of empty city' do
      cli.run
      expect(output.string).to eq "You have to pass a city as parameter\n"
    end
  end

  context 'when city is paris' do
    let(:city) { 'paris' }
    let(:weather) { 'Cloud' }

    before do
      allow(weather_api).to receive(:tomorrow).with(city) { weather }
    end

    it 'returns the weather' do
      cli.run
      expect(output.string).to eq "Tomorrow, it will be #{weather} in #{city}.\n"
    end
  end

  context 'when city is not found' do
    let(:city) { 'montpellier' }

    before do
      allow(weather_api)
        .to receive(:tomorrow).with(city)
        .and_raise(TomorrowWeather::WeatherAPI::CityNotFound)
    end

    it 'failed because of a not valid city' do
      cli.run
      expect(output.string).to eq "The city #{city} is not a valid one!\n"
    end
  end
end
