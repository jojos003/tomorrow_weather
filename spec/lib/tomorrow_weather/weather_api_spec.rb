# frozen_string_literal: true

RSpec.describe TomorrowWeather::WeatherAPI do
  let(:weather_api) { described_class.new }

  describe '#tomorrow' do
    subject(:tomorrow) { weather_api.tomorrow(city) }

    before do
      woeid = described_class::CITY_WOEID[city.to_sym]
      endpoint_url = "#{described_class::BASE_URI}/location/#{woeid}/"

      stub_request(:get, endpoint_url)
        .to_return(body: fixture('paris_20220424.json').read)
    end

    context 'when city is Paris' do
      let(:city) { 'paris' }

      it { is_expected.to eq 'Heavy Cloud' }
    end

    context 'when city is not valid' do
      let(:city) { 'montpellier' }

      it 'raise a CityNotFound error' do
        expect { subject }
          .to raise_error(described_class::CityNotFound)
      end
    end
  end
end
