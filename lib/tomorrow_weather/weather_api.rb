# frozen_string_literal: true

require 'net/http'
require 'json'

module TomorrowWeather
  class WeatherAPI
    class CityNotFound < StandardError; end

    BASE_URI = 'https://www.metaweather.com/api'

    CITY_WOEID = {
      paris: '615702',
      marseille: '610264'
    }.freeze

    def tomorrow(city)
      woeid = CITY_WOEID[city.to_sym]

      raise CityNotFound unless woeid

      json = request("/location/#{woeid}")
      json[:consolidated_weather][1][:weather_state_name]
    end

    private

    def request(endpoint)
      response = Net::HTTP.get(build_uri(endpoint))
      JSON.parse(response, symbolize_names: true)
    end

    def build_uri(endpoint)
      URI("#{BASE_URI}#{endpoint}/")
    end
  end
end
