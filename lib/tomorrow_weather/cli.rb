# frozen_string_literal: true

require 'tomorrow_weather'

module TomorrowWeather
  class CLI
    def initialize(city, output: $stdout, weather_api: WeatherAPI.new)
      @city = city
      @output = output
      @weather_api = weather_api
    end

    def run
      unless @city
        @output.puts('You have to pass a city as parameter')

        return
      end

      weather = @weather_api.tomorrow(@city)
      @output.puts("Tomorrow, it will be #{weather} in #{@city}.")
    rescue WeatherAPI::CityNotFound
      @output.puts "The city #{@city} is not a valid one!"
    end
  end
end
