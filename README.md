Tomorrow Weather
================

Usage
=====

You can use it by cloning the repo:

```sh
$ git clone https://gitlab.com/jojos003/tomorrow_weather.git
$ cd tomorrow_weather
$ bin/tomorrow_weather paris
```

Or by installing the gem:

```sh
$ git clone https://gitlab.com/jojos003/tomorrow_weather.git
$ cd tomorrow_weather
$ gem build
$ gem install tomorrow_weather-0.1.0.gem
$ tomorrow_weather paris
```

Limitations
===========

At the `0.1.0` version, only the cities `paris` and `marseille` are availables.
